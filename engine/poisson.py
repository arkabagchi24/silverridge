import math

import numpy as np

from engine.asian import get_line_pair, is_asian
from engine.models import Odds

MAX_GOALS = 10


def goals_dist(lamda):
    """
    Goals scored follow a Poisson distribution:
    P(k goals) = (λ^k * e^-λ) / k!
    """
    return [(lamda ** k * math.e ** -lamda) / math.factorial(k) for k in
            range(MAX_GOALS)]


def weighted_mean(goals, decay):
    """
    Calculates the weighted mean number of goals scored. Successive earlier
    matches played contribute a smaller value towards the weighted mean.

    :param decay: percentage in which successive matches decrease in importance
    :param goals: list of goals scored
    :return:
    """
    terms = len(goals)
    weights = [1] * terms
    for r in range(terms):
        weights[r] *= (1 - decay)**r
    weights = [w / sum(weights) for w in weights]
    return np.dot(goals, weights)


def make_game_matrix(home_mean, away_mean):
    """Makes a 2D probability matrix using the poisson distributions from
    the home and the away teams.

    Suppose that the probabilities of home goals and away goals are
    h_0, h_1, h_2, ... , h_n and
    a_0, a_1, a_2, ... , a_n
    respectively.

    The outcomes matrix is then given as
    [ h_0 * a_0, h_0 * a_1, ... , h_0 * a_n ]
    [ h_1 * a_0, h_1 * a_1, ... , h_1 * a_n ]
    [ ...
    [ h_n * a_0, h_n * a_1, ... , h_n * a_n ]
    """
    home, away = goals_dist(home_mean), goals_dist(away_mean)
    matrix = np.zeros((MAX_GOALS, MAX_GOALS))
    for h in range(len(home)):
        for a in range(len(away)):
            matrix[h][a] = home[h] * away[a]
    return matrix


def make_game_matrix_without_ties(home_xg, away_xg):
    matrix = make_game_matrix(home_xg, away_xg)
    for g in range(len(matrix) - 1):
        # For all ties, we split the probability of the tie evenly between the
        # home team and the away team. That is, either the home team or the
        # away team has an equal chance to score an additional goal during an
        # overtime period.
        tie_prob = matrix[g][g]
        matrix[g + 1][g] += tie_prob / 2
        matrix[g][g + 1] += tie_prob / 2
        matrix[g][g] = 0
    matrix[-1][-1] = 0
    return matrix


def strength(league_total, team_goals, league_games, team_games):
    league_mean = league_total / league_games
    team_mean = team_goals / team_games
    return team_mean / league_mean


def attack_defense(league_scored, league_allowed, team_scored, team_allowed,
                   league_games, team_games):
    attack = strength(league_scored, team_scored, league_games, team_games)
    defense = strength(league_allowed, team_allowed, league_games, team_games)
    return attack, defense


def xG(home, away, league, league_games, home_games, away_games):
    home_atk, home_def = attack_defense(league[0], league[1], home[0], home[1],
                                        league_games, home_games)
    away_atk, away_def = attack_defense(league[1], league[0], away[0], away[1],
                                        league_games, away_games)

    home_xG = home_atk * away_def * league[0] / league_games
    away_xG = away_atk * home_def * league[1] / league_games
    return home_xG, away_xG


def expected_return(odds, p_win, p_loss=None):
    if p_loss is None:
        p_loss = 1 - p_win
    return (odds - 1) * p_win - p_loss


def outcome_probabilities(matrix):
    home_win, draw, away_win = 0, 0, 0
    for h in range(len(matrix)):
        for a in range(len(matrix[0])):
            if h > a:
                home_win += matrix[h][a]
            if a > h:
                away_win += matrix[h][a]

    # The probability of a draw is the sum of all values along the
    # northwest-southeast diagonal of the probability matrix.
    draw = np.trace(matrix)
    return home_win, draw, away_win


def totals_probabilities(matrix, total):
    under, push = 0, 0
    for h in range(len(matrix)):
        for a in range(len(matrix)):
            if h + a < total:
                under += matrix[h][a]
            if h + a == total:
                push += matrix[h][a]

    # The over is any outcome that is not an under nor a push. We must use
    # complimentary counting here because theoretically the scores could be
    # infinitely large, e.g. 6-6, which we don't calculate.
    over = 1 - (under + push)
    return over, under


def calculate(league, home, away, home_odds, draw_odds, away_odds,
              ou_line, over_odds, under_odds, ties_allowed=True):
    h = (home.scored, home.allowed)
    a = (away.scored, away.allowed)
    l = (league.scored, league.allowed)
    home_xg, away_xg = xG(h, a, l, league.games_played, home.games_played,
                          away.games_played)
    odds = Odds(home_odds, draw_odds, away_odds, ou_line, over_odds, under_odds)
    return calculate_internal(home_xg, away_xg, odds, ties_allowed)

def calculate_internal(home_mean, away_mean, odds, ties_allowed=True):
    if ties_allowed:
        matrix = make_game_matrix(home_mean, away_mean)
    else:
        matrix = make_game_matrix_without_ties(home_mean, away_mean)
    home, draw, away = outcome_probabilities(matrix)

    if is_asian(odds.ou_line):
        ou_lo, ou_hi = get_line_pair(odds.ou_line)
        ou_lo_over, ou_lo_under = totals_probabilities(matrix, ou_lo)
        ou_hi_over, ou_hi_under = totals_probabilities(matrix, ou_hi)
        ev_over = 0.5 * expected_return(odds.over, ou_lo_over,
                                        ou_lo_under) + 0.5 * expected_return(
            odds.over, ou_hi_over, ou_hi_under)
        ev_under = 0.5 * expected_return(odds.under, ou_lo_under,
                                         ou_lo_over) + 0.5 * expected_return(
            odds.under, ou_hi_under, ou_hi_over)
    else:
        over, under = totals_probabilities(matrix, odds.ou_line)
        ev_over = expected_return(odds.over, over, under)
        ev_under = expected_return(odds.under, under, over)
    return {
        'E(home)': expected_return(odds.home, home),
        'E(draw)': expected_return(odds.draw, draw),
        'E(away)': expected_return(odds.away, away),
        'E(over)': ev_over,
        'E(under)': ev_under,
    }
