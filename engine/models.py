class Team(object):
	def __init__(self, name, scored, allowed, games_played):
		self.name = name
		self.scored = scored
		self.allowed = allowed
		self.games_played = games_played


class Odds(object):
	def __init__(self, home, draw, away, ou_line, over, under):
		self.home = home
		self.draw = draw
		self.away = away
		self.ou_line = ou_line
		self.over = over
		self.under = under

def odds_from_dict(odds_dict):
	return Odds(
		float(odds_dict['home_odds']),
		float(odds_dict['draw_odds']),
		float(odds_dict['away_odds']),
		float(odds_dict['ou_line']),
		float(odds_dict['over_odds']), float(odds_dict['under_odds']))
