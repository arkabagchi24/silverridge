import unittest

from engine.poisson import strength


class TestStrengths(unittest.TestCase):
	def setUp(self):
		"""For purposes of these test cases, assume a hypothetical match between
		Manchester City (Home) and Liverpool (Away).

		We use data from the completed 2018-19 Premier League season.
		"""
		self.league_home_goals = 582
		self.league_away_goals = 436
		self.league_matches = 380

	def test_attack_strength(self):
		man_city_atk = strength(
			self.league_home_goals, 57, self.league_matches, 19)
		liverpool_atk = strength(
			self.league_home_goals, 34, self.league_matches, 19)
		self.assertAlmostEqual(man_city_atk, 1.959, delta=0.001)
		self.assertAlmostEqual(liverpool_atk, 1.168, delta=0.001)

	def test_defense_strength(self):
		man_city_def = strength(
			self.league_away_goals, 12, self.league_matches, 19)
		liverpool_def = strength(
			self.league_home_goals, 12, self.league_matches, 19)
		self.assertAlmostEqual(man_city_def, 0.55, delta=0.001)
		self.assertAlmostEqual(liverpool_def, 0.412, delta=0.001)
