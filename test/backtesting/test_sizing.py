import unittest
from backtesting.sizing import kelly

class TestSizing(unittest.TestCase):
    def test_kelly(self):
        """
        Checks that kelly() correctly calculates the bankroll fraction to
        be risked when given a win probability and a bet's multiplicative odds
        (European odds).
        """
        win_probability = 0.6
        odds = 2.0
        self.assertAlmostEqual(kelly(win_probability, odds), 0.2)

    def test_kelly_with_loss_probability(self):
        """
        Checks that kelly() correctly calculates the bankroll fraction
        to be risked when given a win probability, a loss probability not equal
        to the complement of the win probability, and a bet's multiplicative
        odds.
        """
        win_probability = 0.55
        loss_probability = 0.4
        odds = 2.25
        self.assertAlmostEqual(
            kelly(win_probability, odds, p_lose=loss_probability), 0.23)
