import unittest
from backtesting.evaluate import overround, calculate_return


class TestEvaluate(unittest.TestCase):
    def test_overround(self):
        """
        Checks that overround() correctly computes the bookmaker edge on
        a given set of outcomes.
        """
        outcomes = (2.84, 2.97, 3.01)
        self.assertAlmostEqual(overround(outcomes), 0.021, delta=0.001)

    def test_calculate_return_no_bet(self):
        """
        Checks that calculate_return() returns 0 if it does not find an edge on
        a totals bet.
        """
        # 8 May 2023 9:30 Brighton-Everton fixture, O/U 3, 1-5 result
        pnl = calculate_return(-0.05292, 0.006345, 3, 6, 1.94, 1.94)
        self.assertEqual(pnl, 0)

    def test_calculate_return_push(self):
        """
        Checks that calculate_return() returns 0 if it finds an edge on a totals
        bet that pushes (ties).
        """
        # 24 May 2023 10:30 Real Madrid-Rayo Vallecano fixture,
        # O/U 3, 2-1 result
        pnl = calculate_return(0.784318, -0.791226, 3, 3, 2, 1.89)
        self.assertEqual(pnl, 0)

    def test_calculate_return_over_win(self):
        """
        Checks that calculate_return() returns the correct P/L if it finds
        an edge for a winning over goals totals bet.
        """
        over_odds = 2.04

        # 28 May 2023 10:00 Valencia-Espanyol fixture, O/U 2.5, 2-2 result
        pnl = calculate_return(0.346991, -0.364742, 2.5, 4, over_odds, 1.87)
        self.assertEqual(pnl, over_odds - 1)

    def test_calculate_return_over_lose(self):
        """
        Checks that calculate_return() returns -1 if it finds an edge for
        a losing over goals totals bet.
        """
        # 24 May 2023 13:00 Betis-Getafe fixture, O/U 2, 0-1 result
        pnl = calculate_return(0.090564, -0.140645, 2, 1, 2.01, 1.88)
        self.assertEqual(pnl, -1)

    def test_calculate_return_under_win(self):
        """
        Checks that calculate_return() returns the correct P/L if it finds an
        edge for a winning under goals totals bet.
        """
        under_odds = 1.89

        # 21 May 2023 11:45 Udinese-Lazio fixture, O/U 2, 0-1 result
        pnl = calculate_return(-0.15627, 0.092675, 2, 1, 2, under_odds)
        self.assertEqual(pnl, under_odds - 1)

    def test_calculate_return_under_lose(self):
        """
        Checks that calculate_return() returns -1 if it finds an edge for a
        losing under goals totals bet.
        """
        # 21 May 2023 9:00 Napoli-Inter fixture, O/U 2.5, 3-1 result
        pnl = calculate_return(-0.446938, 0.425753, 2.5, 4, 1.88, 2.02)
        self.assertEqual(pnl, -1)
