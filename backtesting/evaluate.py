from engine.asian import is_asian, get_line_pair


def overround(outcomes):
    """Calculates the house edge, or "overround" that a bookmaker levies.

    This is given by the value of which the sum of the implied probabilities
    exceeds 100%.

    For instance, assume the following:
    * Home Odds: 2.84   (35.2% implied win probability)
    * Draw Odds: 2.97   (33.7% implied win probability)
    * Away Odds: 3.01   (33.2% implied win probability)

    The sum of all implied probabilities is 102.1%,
    yielding an overround of 2.1%.
    """
    return sum(1 / odds for odds in outcomes) - 1


def calculate_return(e_over, e_under, ou_line, total_goals,
                     over_odds, under_odds):
    bet_pushes = total_goals == ou_line
    if bet_pushes: return 0

    edge = overround((over_odds, under_odds))

    # Only make bets if the expected value is significant.
    if e_over > edge:
        return over_odds - 1 if total_goals > ou_line else -1
    if e_under > edge:
        return under_odds - 1 if total_goals < ou_line else -1
    return 0


def calculate_moneyline_return(e_home, e_draw, e_away, home_goals, away_goals,
                               home_odds, draw_odds, away_odds):
    edge = overround((home_odds, draw_odds, away_odds))
    if e_home > edge and kelly_criterion(e_home, home_odds) > 0:
        units = kelly_criterion(e_home, home_odds)
        if home_goals > away_goals:
            return units, units * (home_odds - 1)
        return units, -units
    if e_draw > edge and kelly_criterion(e_draw, draw_odds) > 0:
        units = kelly_criterion(e_draw, draw_odds)
        if home_goals == away_goals:
            return units, units * (draw_odds - 1)
        return units, -units
    if e_away > edge:
        units = kelly_criterion(e_away, away_odds)
        if away_goals > home_goals and kelly_criterion(e_away, away_odds) > 0:
            return units, units * (away_odds - 1)
        return units, -units
    return 0, 0

def calculate_moneyline_return_flat(e_home, e_draw, e_away, home_goals, away_goals,
                               home_odds, draw_odds, away_odds):
    edge = overround((home_odds, draw_odds, away_odds))
    if e_home > edge:
        if home_goals > away_goals:
            return home_odds - 1
        return -1
    if e_draw > edge:
        if home_goals == away_goals:
            return draw_odds - 1
        return -1
    if e_away > edge:
        if away_goals > home_goals:
            return away_odds - 1
        return -1
    return 0


def _get_probability(ev, odds):
    return (1 + ev) / odds


def kelly_criterion(ev, odds):
    p_win = _get_probability(ev, odds)
    p_loss = 1 - p_win
    return p_win - p_loss / (odds - 1)


def evaluate(df):
    df['total_goals'] = df['home_goals'] + df['away_goals']
    df = df[df['E(home)'] != -1]
    df = df[df['E(away)'] != -1]

    pnl = []
    ml_pnl = []
    kelly_ml_pnl = []
    for _, row in df.iterrows():
        goals = row['total_goals']
        ou_line = row['ou_line']
        if is_asian(ou_line):
            line1, line2 = get_line_pair(ou_line)
            units = 0.5 * calculate_return(row['E(over)'], row['E(under)'], line1, goals, row['over_odds'],
                row['under_odds']) + 0.5 * calculate_return(row['E(over)'], row['E(under)'], line2, goals, row['over_odds'],
                row['under_odds'])
            pnl.append(units)
        else:
            pnl.append(calculate_return(
                row['E(over)'], row['E(under)'], float(ou_line), goals, row['over_odds'],
                row['under_odds']
            ))
        ml_pnl.append(calculate_moneyline_return_flat(
            row['E(home)'],
            row['E(draw)'],
            row['E(away)'],
            row['home_goals'],
            row['away_goals'],
            row['home_odds'],
            row['draw_odds'],
            row['away_odds']
        ))
        kelly_ml_pnl.append(calculate_moneyline_return(
            row['E(home)'],
            row['E(draw)'],
            row['E(away)'],
            row['home_goals'],
            row['away_goals'],
            row['home_odds'],
            row['draw_odds'],
            row['away_odds']
        ))
    df['Totals P/L'] = pnl
    df['Moneylines P/L'] = ml_pnl
    df['Units Risked (ML-Kelly)'] = list(map(lambda t: t[0], kelly_ml_pnl))
    df['Moneylines P/L (Kelly)'] = list(map(lambda t: t[1], kelly_ml_pnl))
    return df
