from datetime import datetime, timedelta

import matplotlib.pyplot as plt
import pandas as pd

from backtesting.evaluate import evaluate
from common.constants import DATE_FORMAT
from common.utils import make_data
from common.commandline import make_arguments
from engine.models import odds_from_dict
from engine.poisson import weighted_mean, calculate_internal


def format_date(string):
    return datetime.strptime(string, DATE_FORMAT)


def get_previous_matches(kickoff, matches, team, is_home):
    kickoff = format_date(kickoff)
    cutoff = kickoff - timedelta(hours=4)
    previous = []
    i = 0
    while format_date(matches[i]['kickoff']) < cutoff:
        if is_home and matches[i]['home_team'] == team:
            previous.append(matches[i])
        elif not is_home and matches[i]['away_team'] == team:
            previous.append(matches[i])
        elif team == 'league':
            previous.append(matches[i])
        i += 1
    return previous


def calculate(match, matches, team, is_home, decay):
    prev = get_previous_matches(match['kickoff'], matches, team,
                                is_home=is_home)
    prev.reverse()
    home_goals = [int(m['home_goals']) for m in prev]
    away_goals = [int(m['away_goals']) for m in prev]
    return weighted_mean(home_goals, decay), weighted_mean(away_goals, decay)

def get_evs(match, odds, matches, decay):
    league_h_wt_mean, league_a_wt_mean = calculate(match,
                                                   matches,
                                                   'league', True,
                                                   decay)
    if league_h_wt_mean == 0 or league_a_wt_mean == 0:
        return
    h_scored, h_allowed = calculate(match, matches,
                                    match['home_team'], True, decay)
    h_scored /= league_h_wt_mean
    h_allowed /= league_a_wt_mean

    a_allowed, a_scored = calculate(match, matches,
                                    match['away_team'], False, decay)
    a_allowed /= league_h_wt_mean
    a_scored /= league_a_wt_mean

    h_xg = h_scored * a_allowed * league_h_wt_mean
    a_xg = a_scored * h_allowed * league_a_wt_mean
    return calculate_internal(h_xg, a_xg, odds)

def run_iteration(matches, decay):
    data = {
        'kickoff': [],
        'home_team': [], 'away_team': [],
        'home_goals': [], 'away_goals': [],
        'E(home)': [], 'E(draw)': [], 'E(away)': [],
        'E(over)': [], 'E(under)': [],
        'home_odds': [], 'draw_odds': [], 'away_odds': [],
        'ou_line': [],
        'over_odds': [], 'under_odds': [],
    }

    for match in matches:
        kickoff = match['kickoff']
        try:
            odds_obj = odds_from_dict(match)
            evs = get_evs(match, odds_obj, matches, decay)
            if not evs:
                continue
            data['kickoff'].append(kickoff)
            data['home_team'].append(match['home_team'])
            data['away_team'].append(match['away_team'])
            data['home_goals'].append(int(match['home_goals']))
            data['away_goals'].append(int(match['away_goals']))
            for k, v in evs.items():
                data[k].append(v)
            data['home_odds'].append(odds_obj.home)
            data['draw_odds'].append(odds_obj.draw)
            data['away_odds'].append(odds_obj.away)
            data['ou_line'].append(odds_obj.ou_line)
            data['over_odds'].append(odds_obj.over)
            data['under_odds'].append(odds_obj.under)
        except KeyError:
            continue
        except ZeroDivisionError:
            continue

    df = pd.DataFrame.from_dict(data)
    bt_df = evaluate(df.round(6))
    return bt_df[['Totals P/L', 'Moneylines P/L', 'Units Risked (ML-Kelly)', 'Moneylines P/L (Kelly)']]


def plot_pnl(pnls):
    decays = list(range(25+1))
    plt.bar(x=decays, height=pnls)
    plt.title('P/L vs. Decay')
    plt.xlabel('Decay (%)')
    plt.ylabel('P/L (units)')
    plt.axhline(y=0, color='r', linestyle='-')
    plt.show()


def plot_drifts(drifts):
    for decay, drift in drifts.items():
        plt.plot(drift, label=f'd={decay}')
    plt.axhline(y=0, color='r', linestyle='-')
    plt.legend()
    plt.show()


if __name__ == '__main__':
    args = make_arguments()
    league = args.league
    season = args.season
    data_file = f'data/{league}-{season}.csv'
    matches = make_data(data_file)

    iterations = 25
    pnls = []
    ml_pnls = []
    drifts = {}
    pnl_data = {
        'decays': list(0.01 * d for d in range(iterations + 1)),
        'Totals P/L': [],
        'Moneylines P/L': [],
        'Moneylines P/L (Kelly)': [],
        'Units Risked (ML-Kelly)': [],
    }
    for itr in range(iterations + 1):
        decay = 0.01 * itr
        pnl = run_iteration(matches, decay)
        total_pnl = round(pnl['Totals P/L'].sum(), 3)
        ml_pnl = round(pnl['Moneylines P/L'].sum(), 3)
        kelly_ml_pnl = round(pnl['Moneylines P/L (Kelly)'].sum(), 5)
        kelly_ml_units = round(pnl['Units Risked (ML-Kelly)'].sum(), 5)

        pnls.append(total_pnl)
        ml_pnls.append(ml_pnl)

        pnl_data['Totals P/L'].append(total_pnl)
        pnl_data['Moneylines P/L'].append(ml_pnl)
        pnl_data['Moneylines P/L (Kelly)'].append(kelly_ml_pnl)
        pnl_data['Units Risked (ML-Kelly)'].append(kelly_ml_units)

        drifts[decay] = pnl['Totals P/L'].cumsum().tolist()
        print(f'd={decay}\tP/L={total_pnl}')
    pnl_df = pd.DataFrame.from_dict(pnl_data).set_index('decays')
    print(pnl_df.round(6).to_csv(sep='\t'))
    plot_pnl(pnls)
    plot_drifts(drifts)
