import csv
import random
import time
import urllib.parse

from common.constants import BASE_URL, LEAGUES, DATE_FORMAT
from common.commandline import make_arguments
from scraping import match_urls, over_under_odds, results, ternary_odds


def format_data(home, away, ternary, result, ou_odds):
    ou_line = list(ou_odds.keys())[0]
    ou_tuple = [ou_line] + ou_odds[ou_line]
    kickoff = result['start'].strftime(DATE_FORMAT)
    return [kickoff, home, away, result['home'], result['away']] + ternary + ou_tuple


def write_to_csv(filename, data, header):
    with open(filename, 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(header)
        writer.writerows(data)


# TODO(rohit): MLS special case?
# TODO(rohit): Current season workaround
if __name__ == '__main__':
    args = make_arguments()
    league = args.league
    country = LEAGUES[league]
    season = args.season

    # Current season workaround:
    # results_url = f'https://www.betexplorer.com/soccer/{country}/{league}/results/'

    results_url = f'https://www.betexplorer.com/football/{country}/{league}-{season}/results/'
    matchpages = match_urls.get(results_url)

    # Sort matchpages by earliest to latest date.
    matchpages.reverse()

    data = []
    for matchpage in matchpages:
        home = matchpage[0]
        away = matchpage[1]
        match_url = urllib.parse.urljoin(BASE_URL, matchpage[2])
        print(home, away, match_url)

        ternary = ternary_odds.get(match_url)
        result = results.get(match_url)
        ou_odds = over_under_odds.get(match_url)
        if not ternary or not ou_odds:
            print(f'Skipping {home} - {away}.')
            continue
        data.append(format_data(home, away, ternary, result, ou_odds))

        # Avoid rate-limiting.
        delay_sec = 0.3 + 0.1 * random.randint(0, 5)
        time.sleep(delay_sec)

    data_header = ['kickoff',
                   'home_team', 'away_team', 'home_goals', 'away_goals',
                   'home_odds', 'draw_odds', 'away_odds',
                   'ou_line', 'over_odds', 'under_odds']
    write_to_csv(f'{league}-{season}.csv', data, data_header)
