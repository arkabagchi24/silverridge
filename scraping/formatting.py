from common.constants import DATE_FORMAT


def format_row(home, away, ternary, start, ou_odds):
    ou_line = list(ou_odds.keys())[0]
    ou_tuple = [ou_line] + ou_odds[ou_line]
    kickoff = start.strftime(DATE_FORMAT)
    return [kickoff, home, away] + ternary + ou_tuple
