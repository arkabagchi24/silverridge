import datetime

import pytz
import requests
from bs4 import BeautifulSoup


def get_start(soup):
    start = soup.find_all('p', {'class': 'list-details__item__date'})[0].attrs[
        'data-dt']
    start_time = datetime.datetime.strptime(start + ' +0100',
                                            '%d,%m,%Y,%H,%M %z')
    return start_time.astimezone(pytz.utc)
    # return start_time


def get(match_url):
    r = requests.get(match_url)
    soup = BeautifulSoup(r.text, 'html.parser')

    start_time = get_start(soup)
    full_time = soup.find_all('p', {'class': 'list-details__item__score'})
    if not full_time:
        return None
    full_time = full_time[0].text
    return {
        'start': start_time,
        'home': int(full_time.split(':')[0]),
        'away': int(full_time.split(':')[1]),
    }
