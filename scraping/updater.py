import csv
import hashlib
import random
import time
import urllib

from common.constants import LEAGUES, BASE_URL, DATE_FORMAT
from common.utils import make_data
from common.commandline import make_arguments
from scraping import match_urls, ternary_odds, over_under_odds, results


def format_result(home, away, result):
    kickoff = result['start'].strftime(DATE_FORMAT)
    return [kickoff, home, away, result['home'], result['away']]

def format_data(home, away, ternary, result, ou_odds):
    ou_line = list(ou_odds.keys())[0]
    ou_tuple = [ou_line] + ou_odds[ou_line]
    kickoff = result['start'].strftime(DATE_FORMAT)
    return [kickoff, home, away, result['home'], result['away']] + ternary + ou_tuple


def get_match_hash(kickoff, home, away):
    return hashlib.sha512(f'{kickoff}-{home}-{away}'.encode()).hexdigest()


def add(rows, csvfile):
    with open(csvfile, 'a') as f:
        writer = csv.writer(f)
        writer.writerows(rows)


if __name__ == '__main__':
    args = make_arguments()
    league = args.league
    country = LEAGUES[league]
    season = args.season

    matches_file = f'backtesting/data/{league}-{season}.csv'
    results_data = make_data(matches_file)
    s_kickoff = results_data[-1]['kickoff']
    s_home = results_data[-1]['home_team']
    s_away = results_data[-1]['away_team']
    sentinel_hash = get_match_hash(s_kickoff, s_home, s_away)

    # Current season workaround:
    results_url = f'https://www.betexplorer.com/football/{country}/{league}/results/'
    matchpages = match_urls.get(results_url)

    upcomings = []
    for matchpage in matchpages:
        home = matchpage[0]
        away = matchpage[1]
        match_url = urllib.parse.urljoin(BASE_URL, matchpage[2])
        print(home, away, match_url)

        ternary = ternary_odds.get(match_url)
        result = results.get(match_url)
        ou_odds = over_under_odds.get(match_url)

        if not ternary or not ou_odds:
            print(f'Skipping {home} - {away}.')
            continue
        r_line = format_result(home, away, result)
        match_hash = get_match_hash(r_line[0], r_line[1], r_line[2])
        if match_hash == sentinel_hash:
            break

        upcomings.append(format_data(home, away, ternary, result, ou_odds))

        # Avoid rate-limiting.
        delay_sec = 0.3 + 0.1 * random.randint(0, 5)
        time.sleep(delay_sec)

    # Sort the results and odds by earliest to latest, since we traversed the
    # matchpages from most recent backwards.
    upcomings.reverse()
    add(upcomings, matches_file)
