import over_under_odds
import results
import ternary_odds

if __name__ == '__main__':
    match_url = ' https://www.betexplorer.com/soccer/england/premier-league/newcastle-utd-brighton/Q1SWq2U0/'
    ternary = ternary_odds.get(match_url)
    result = results.get(match_url)
    ou_odds = over_under_odds.get(match_url)
    print(ternary)
    print(result)
    print(ou_odds)
