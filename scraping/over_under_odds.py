import os

import bs4

from common.utils import parse_html, format_float
from common.constants import BOOKMAKER_ID, ODDS_TAG, OU_LINE_TAG


def _make_over_under_odds_url(match_id):
    return f'https://www.betexplorer.com/match-odds/{match_id}/1/ou/'


def _parse_ou_odds(url):
    soup = parse_html(url)

    odds = soup.find_all('tr', {'data-bid': BOOKMAKER_ID})
    if not odds:
        return None
    ou_lines = {}
    for odd in odds:
        children = [c for c in odd.children if isinstance(c, bs4.Tag)]
        for c in children:
            if OU_LINE_TAG in c.get_attribute_list('class'):
                ou_line = format_float(c.contents[0])
                ou_lines[ou_line] = []
            elif c.has_attr(ODDS_TAG):
                ou_lines[ou_line].append(format_float(c[ODDS_TAG]))
    return find_minimum_spread(ou_lines)


def get(match_url):
    if match_url.endswith('/'):
        match_url = match_url[:-1]
    subpages = os.path.split(match_url)
    match_id = subpages[-1]

    url = _make_over_under_odds_url(match_id)
    return _parse_ou_odds(url)


def find_minimum_spread(ou_lines):
    min_spread = 10000
    for line in ou_lines.keys():
        over = ou_lines[line][0]
        under = ou_lines[line][1]
        spread = abs(over - under)

        # TODO(rohit): What happens in the case of ties between line spreads?
        if spread < min_spread:
            best_line = line
            min_spread = spread
    return {best_line: ou_lines[best_line]}
