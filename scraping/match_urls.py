import requests
from bs4 import BeautifulSoup


def get(results_url):
    r = requests.get(results_url)
    soup = BeautifulSoup(r.text, 'html.parser')

    tags = soup.find_all('a', {'class': 'in-match'})
    matchpages = []
    for tag in tags:
        home = tag.contents[0].text
        away = tag.contents[2].text
        match_url = tag.attrs['href']
        matchpages.append((home, away, match_url))
    return matchpages
