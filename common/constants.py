BASE_URL = 'https://www.betexplorer.com/'
OU_LINE_TAG = 'table-main__doubleparameter'
ODDS_TAG = 'data-odd'
BOOKMAKER_ID = '18'  # Pinnacle

LEAGUES = {
    '2-bundesliga': 'germany',
    'bundesliga': 'germany',
    'championship': 'england',
    'eredivisie': 'netherlands',
    'liga-portugal': 'portugal',
    'ligue-1': 'france',
    'laliga': 'spain',
    'laliga2': 'spain',
    # The MLS is a special snowflake not yet supported.
    'mls': 'usa',
    'serie-a': 'italy',
    'super-lig': 'turkey',
    'premier-league': 'england',
}

DATE_FORMAT = '%d %b %Y %H:%M'
