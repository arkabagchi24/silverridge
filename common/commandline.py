import argparse


def make_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--league', type=str, required=True,
                        help='Name of league')
    parser.add_argument('-s', '--season', type=str, required=True,
                        help='Season or year, e.g. 2021-2022')
    return parser.parse_args()
